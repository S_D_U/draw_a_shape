﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject cursorPrefab;
	public GameObject touchPointPrefab;
	public GameObject startButton;
	public GameObject restartButton;
	public GameObject []shapesPrefab;
	public Text scoreText;
	public Text timerText;

	private float width, height, k;
	private GameObject cursor;
	private GameObject touchPoint;
	private GameObject shape;
	private Vector3 lastPos, newPos;
	private bool play = false;
	private int i,j;
	private int score;
	private float timer;
	private float roundTime;

	void Start () {
		width = Screen.width;
		height = Screen.height;
		k = height / 10f;
		lastPos = new Vector3 ();
		newPos = new Vector3 ();
	}

	void Update () {
		if (play) {
			if (Input.GetMouseButtonDown (0)) {				
				lastPos.x = (Input.mousePosition.x - width / 2) / k;
				lastPos.y = (Input.mousePosition.y - height / 2) / k;
				cursor = Instantiate (cursorPrefab, lastPos, Quaternion.identity) as GameObject;
				shape.GetComponent<ShapeController> ().ShapeScale ();
			}

			if (Input.GetMouseButton (0) && cursor != null) {
				newPos.x = (Input.mousePosition.x - width / 2) / k;
				newPos.y = (Input.mousePosition.y - height / 2) / k;
				cursor.transform.position = newPos;            
				lastPos = newPos;
			}

			if (Input.GetMouseButtonUp (0) && cursor != null) {
				CheckPointController.k = 0;
				RoundRestart ();
			}
		}
	}

	void FixedUpdate () {
		if (play) {
			timer -= Time.deltaTime;
			if (timer < 0) {
				timer = 0;
				CleanScreen ();
				play = false;
				timerText.gameObject.SetActive (false);
				scoreText.text = "Score: " + score;
				scoreText.gameObject.SetActive (true);
				restartButton.SetActive (true);
			}
			timerText.text = "Time: " + timer.ToString ("F");
		}
	}

	public void CreateTouchPoint (float scale) {		
		touchPoint = Instantiate (touchPointPrefab, lastPos, Quaternion.identity) as GameObject;
		touchPoint.transform.localScale = new Vector3 (scale, scale, 1f);
	}

	public void NewGame() {
		if (startButton.activeSelf) {
			startButton.SetActive (false);
		}
		if (restartButton.activeSelf) {
			restartButton.SetActive (false);
			scoreText.gameObject.SetActive (false);
		}
		timerText.gameObject.SetActive (true);
		NewShape (true);
		play = true;
		roundTime = 20f;
		timer = roundTime;
		score = 0;
	}

	private void NewShape (bool newShape){
		if (newShape) {
			do {
				j = Random.Range (0, shapesPrefab.Length);		
			} while(i == j);
			i = j;
		}
		shape = Instantiate (shapesPrefab [i], new Vector3 (), Quaternion.identity) as GameObject;
	}

	public void RoundRestart() {
		CleanScreen ();
		NewShape (false);
	}

	public void NewRound(){		
		CleanScreen ();
		NewShape (true);
		score++;
		roundTime -= 1f;
		timer = roundTime;
	}

	private void CleanScreen(){
		Destroy (touchPoint);
		Destroy (cursor);
		Destroy (shape);
	}

	public void Exit(){
		Application.Quit ();
	}
}
