﻿using UnityEngine;
using System.Collections;

public class TouchPointController : MonoBehaviour {

	private bool exit = false;

	void OnTriggerExit2D(Collider2D other) {		
		exit = true;
	}

	void OnTriggerStay2D(Collider2D other) {
		if (exit && CheckPointController.k >= 4) {			
			Camera.main.GetComponent<GameController>().NewRound ();
			CheckPointController.k = 0;
		}
	}
}
