﻿using UnityEngine;
using System.Collections;

public class ShapeController : MonoBehaviour {

	private bool enter = false;
	private bool touchDown = false;
	private Vector3 scale;

	void OnTriggerEnter2D(Collider2D other) {
		enter = true;
		Camera.main.GetComponent<GameController> ().CreateTouchPoint (scale.x/5);
	}

	void OnTriggerExit2D(Collider2D other) {
		Camera.main.GetComponent<GameController> ().RoundRestart ();
		CheckPointController.k = 0;
	}

	public void ShapeScale(){
		touchDown = true;
	}

	void Update() {
		if (touchDown && !enter) {			
			scale = gameObject.transform.localScale;
			scale = new Vector3 (scale.x + 0.5f, scale.y + 0.5f, scale.z);
			gameObject.transform.localScale = scale;
			if (scale.x > 9) touchDown = false;
		} else
			touchDown = false;		
	}
}
