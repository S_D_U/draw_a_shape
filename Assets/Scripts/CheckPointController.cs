﻿using UnityEngine;
using System.Collections;

public class CheckPointController : MonoBehaviour {

	public static byte k = 0;
	private bool active = true;

	void OnTriggerEnter2D(Collider2D other) {
		if (active) {
			k++;
			Debug.Log (k);
			active = false;
		}
	}
}
